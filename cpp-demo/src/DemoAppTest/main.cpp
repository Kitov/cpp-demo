#define CATCH_CONFIG_MAIN
#include <catch.hpp>


/*----------------------------------*/
class SmaFilter_t{
private:
    int Acumulator;
    int Counter;
public:
    SmaFilter_t() {
        this->Acumulator = 0;
        this->Counter = 0;
    }
    void Add(unsigned int val) {
        this->Acumulator += val;
        this->Counter++;
    }
    int Get(void) {
        return (this->Acumulator/this->Counter);
    }
};
/*----------------------------------*/

TEST_CASE("DemoAppTest") {
	int a = 0;
	int b = 1;
	CHECK(a != b);
}
TEST_CASE("SmaTest0") {
    SmaFilter_t Tmp;
    Tmp.Add(10);
    Tmp.Add(2);
    CHECK( Tmp.Get() == 6);
}
TEST_CASE("SmaTest1") {
    SmaFilter_t Tmp;
    Tmp.Add(5);
    Tmp.Add(6);
    Tmp.Add(4);
    CHECK( Tmp.Get() == 5);
}
