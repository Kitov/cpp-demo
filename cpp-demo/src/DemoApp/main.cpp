#include <iostream>

#include "stdint.h"

void f() {
}

class SmaFilter_t{
private:
    int Acumulator;
    int Counter;
public:
    SmaFilter_t() {
        this->Acumulator = 0;
        this->Counter = 0;
    }
    void Add(unsigned int val) {
        this->Acumulator += val;
        this->Counter++;
    }
    int Get(void) {
        return (this->Acumulator/this->Counter);
    }
};


int main() {
    SmaFilter_t AvgVoltage;

    AvgVoltage.Add(2);
    AvgVoltage.Add(10);

    std::cout << "Hello John!\n" << AvgVoltage.Get();

    return 0;
}
